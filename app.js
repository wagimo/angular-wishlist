
require('dotenv').config();
const express = require('express');
const cors = require('cors');

// INICIANDO EL SERVIDOR WEB
const app = express();

//CONFIGURANDO CORS
app.use(cors());

//LECTURA DEL CONTENIDO DEL BODY EN FORMATO JSON
app.use(express.json());

//DEFINIENDO LAS RUTAS POR DEFECTO PARA EL MODULO DE DESTINOS
app.use('/api/destinos', require('./routes/destinos'));

//DEFINIENDO LAS RUTAS POR DEFECTO PARA EL MODULO DE CIUDADES
app.use('/api/ciudades', require('./routes/ciudades'));

// DEFINIENDO LAS RUTAS PARA EL MODULO DE TRADUCCIONES
app.use('/api/translation', require('./routes/translation'));


app.listen(process.env.PORT, ()=> console.log('Servidor escuchando en el puerto ' + process.env.PORT));

