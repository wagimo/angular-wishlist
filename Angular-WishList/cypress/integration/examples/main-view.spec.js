
describe('Home Sitio Web', () => {
  beforeEach(() => cy.visit('http://localhost:451/home/destinos'));
  context('When: ingrese a la pagina de destinos', () => {
    it('THEN: Debería desplegar el titulo principal de la ventana', () =>{
      cy.contains('Agencia de Viajes');
    });

    it('THEN: Debería de desplegar el nombre de la agencia de viajes en la ventana', ()=>{
      cy.get('h2 span').should('contain','Sueño de Viajar')
    });


  })

})
