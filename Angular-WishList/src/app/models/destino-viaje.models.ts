export class Destino {
  private selected: boolean;
  public servicios: string[];

  constructor(public nombre: string, public imagen: string) {
    this.servicios = ['Desayuno', 'Almuerzo', 'Comida', 'Licores'];
  }

  isSelected(): boolean {
    return this.selected;
  }

  setSelected(select: boolean): void {
    this.selected = select;
  }
}
