import { Servicios } from './servicios.interface';

export interface Destino
{
    id: string;
    nombre: string;
    imagen: string;
    selected: boolean;
    servicios: Servicios[];
    votos: number;
    precio?: number;
    trackClicUp?: number;
    trackClicDown?: number;
}
