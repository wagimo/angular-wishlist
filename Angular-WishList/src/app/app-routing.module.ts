import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesnotfoundComponent } from './pagesnotfound/pagesnotfound.component';
import { ComponentRoutingModule } from './components/component-routing.module';
import { SecurityRoutingModule } from './security/security-routing.module';

const routes: Routes = [
  {
    path: '',
    redirectTo:'/home',
    pathMatch:'full'
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./components/component.module').then((m) => m.ComponentModule),
  },
  {
    path: 'vuelos',
    loadChildren: () =>
      import('./vuelos/vuelos.module').then((m) => m.VuelosModule),
  },
  {
    path: '**',
    component: PagesnotfoundComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    ComponentRoutingModule,
    SecurityRoutingModule,
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
