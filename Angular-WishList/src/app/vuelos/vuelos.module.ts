import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VuelosRoutingModule } from './vuelos-routing.module';
import { VuelosComponent } from './vuelos.component';
import { MainComponent } from './main/main.component';
import { MasinfoComponent } from './masinfo/masinfo.component';
import { DetalleComponent } from './detalle/detalle.component';
import { ApiDestinosServices } from '../services/api-destinos-services.service';


@NgModule({
  declarations: [VuelosComponent, MainComponent, MasinfoComponent, DetalleComponent],
  imports: [
    CommonModule,
    VuelosRoutingModule
  ],
  providers: [ApiDestinosServices],
})
export class VuelosModule { }
