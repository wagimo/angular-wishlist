import { Component, OnInit } from '@angular/core';
import { ApiDestinosServices } from '../../services/api-destinos-services.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';



export class ApiDestinosServicesOld{
  GetAllDestinos(): Observable<any[]>{
    const obs$ = new Observable<any[]>(s => s.next([{
      id: 1,
      pais: 'España OLD',
      ciudad: 'Madrid',
      tiempo: '8h',
      valor: '1500 Usd'
    },
    {
      id: 2,
      pais: 'España OLD',
      ciudad: 'Barcelona',
      tiempo: '10h',
      valor: '2000 Usd'
    }
  ]));
    return obs$;
  }
}

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styles: []
})
export class DetalleComponent implements OnInit {

  destinoId: number;
  vuelo: any = {};

  constructor(
    private services: ApiDestinosServices,
    private rutaActiva: ActivatedRoute
    ) { }

  ngOnInit(): void {

    this.rutaActiva.params.subscribe( params => {
      this.destinoId = params.id;
    });



    if (this.destinoId !== undefined){

      this.services.GetVuelosById(this.destinoId).subscribe( response => {

        this.vuelo = response;
      });
    }
  }

}
