import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VuelosComponent } from './vuelos.component';
import { MainComponent } from './main/main.component';
import { MasinfoComponent } from './masinfo/masinfo.component';
import { DetalleComponent } from './detalle/detalle.component';
import { UsuarioLogueadoGuard } from '../guards/usuarioLogueado/usuario-logueado.guard';

const routes: Routes = [
  {
    path: '',
    component: VuelosComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: [
      {
        path: '',
        redirectTo: 'main',
        pathMatch: 'full',
        data: { title: 'Vuelos Disponibles' },
      },
      {
        path: 'main',
        component: MainComponent,
      },
      {
        path: 'info',
        component: MasinfoComponent,
      },
      {
        path: 'detalle/:id',
        component: DetalleComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VuelosRoutingModule {}
