import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiDestinosServices } from '../../services/api-destinos-services.service';
import { Injectable } from '@angular/core';



export class ApiDestinosServicesOld{
  GetAllDestinos(): Observable<any[]>{
    const obs$ = new Observable<any[]>(s => s.next([{
      id: 1,
      pais: 'España ',
      ciudad: 'Madrid OLD',
      tiempo: '8h',
      valor: '1500 Usd'
    },
    {
      id: 2,
      pais: 'España ',
      ciudad: 'Barcelona OLD',
      tiempo: '10h',
      valor: '2000 Usd'
    }
  ]));
    return obs$;
  }
}

@Injectable()
export class ApiDestnosDecdorated extends ApiDestinosServices{
  GetAllDestinos(): Observable<any[]>{
    const obs$ = new Observable<any[]>(s => s.next([{
      id: 1,
      pais: 'España ',
      ciudad: 'Madrid DECORED CLASS',
      tiempo: '8h',
      valor: '1500 Usd'
    },
    {
      id: 2,
      pais: 'España ',
      ciudad: 'Barcelona DECORED CLASS',
      tiempo: '10h',
      valor: '2000 Usd'
    },
    {
      id: 3,
      pais: 'Mexico ',
      ciudad: 'Cancún DECORED CLASS',
      tiempo: '5h',
      valor: '1200 Usd'
    }
  ]));
    return obs$;
  }
}


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styles: [],
//   providers: [
//   {
//      provide: ApiDestinosServices,
//      useClass: ApiDestnosDecdorated
//   },
//   {
//     provide: ApiDestinosServicesOld,
//     useExisting: ApiDestinosServices
//   }
// ]
})
export class MainComponent implements OnInit {

  vuelos: any[] = [];

  constructor(private services: ApiDestinosServices) {

   }

  ngOnInit(): void {

    this.services.GetVuelos().subscribe(response => {
      this.vuelos = response;
    });
  }

}

