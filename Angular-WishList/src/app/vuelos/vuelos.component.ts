import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vuelos',
  templateUrl: './vuelos.component.html',
  styleUrls: []
})
export class VuelosComponent implements OnInit {




  constructor(private router: Router ) { }

  ngOnInit(): void {
  }

  salir(): void{
    localStorage.removeItem('token');
    this.router.navigateByUrl('/login');
  }
}
