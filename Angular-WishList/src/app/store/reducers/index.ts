import * as fromDestinoreduce from './app.reducer';

// DEFINIMOS EL OBJETO QUE REPRESENTA EL ESTADO DE LA APLICACION

export interface AppState{
  destinos: fromDestinoreduce.DestinosState;
}

// DEFINIMOS UNA CONSTANTE QUE ALMACENA UN OBJETO EL CUAL TIENE UNA PROPIEDAD QUE REFERENCIA A LA FUNCION REDUCER
export const reducer = {
  destinos: fromDestinoreduce.reducer
};



