import * as fromDestinosAction from '../actions/destino.action';
import { Destino } from '../../models/destino.interface';

// MODELO PARA DESTINO STATE
export interface DestinosState {
  data: Destino[];
  loading: boolean;
  loaded: boolean;
  error: string;
  favorito: Destino;
}

// Estado inicial de la aplicación
export const initialState: DestinosState = {
  data: [],
  loading: false,
  loaded: false,
  error: '',
  favorito: null,
};

export function reducer(
  state = initialState,
  action: fromDestinosAction.DestinosActions
): DestinosState {

  switch (action.type) {
    case fromDestinosAction.LOAD_DESTINO_SECCESS: {
      const data = (action as fromDestinosAction.LoadDestinoSuccess).payload;
      return {
        ...state,
        loading: false,
        loaded: true,
        data,
      };
    }
    case fromDestinosAction.LOAD_FAILED: {
      return {
        ...state,
        loaded: false,
        loading: false,
        error: action.payload,
      };
    }

    case fromDestinosAction.ADD_DESTINO_SUCCESS: {
      return {
        ...state,
        data: [...state.data, action.payload],
        favorito: null,
      };
    }

    case fromDestinosAction.ADD_DESTINO_FAIL: {
      return {
        ...state,
        error: action.payload,
      };
    }

    case fromDestinosAction.UPDATE_FAVORITO_DESTINO_SUCCESS: {
      const data = state.data.map((destino) => {
        if (destino.id === action.payload.id) {
          return action.payload;
        } else {
          return { ...destino, selected: false };
        }
      });

      return {
        ...state,
        data,
        loaded: true,
        loading: false,
        favorito: action.payload,
      };
    }

    case fromDestinosAction.ELIMINAR_DESTINO_SUCCESS: {
      const data = state.data.filter((destino) => {
        return destino.id !== action.payload;
      });

      return {
        ...state,
        data,
        loaded: true,
        loading: false,
      };
    }

    case fromDestinosAction.ELIMINAR_DESTINO_FAIL: {
      return {
        ...state,
        error: action.payload,
      };
    }

    case fromDestinosAction.VOTAR_DESTINO_SUCCESS: {
      const data = state.data.map((destino) => {
        if (destino.id === action.payload.id) {
          return action.payload;
        } else {
          return { ...destino };
        }
      });

      return {
        ...state,
        data,
        loaded: true,
        loading: false,
        favorito: null,
      };
    }

    case fromDestinosAction.RESET_VOTOS_DESTINO_SUCCESS: {
      const data = state.data.map((destino) => {
        if (destino.votos !== 0) {
          return { ...destino, votos: 0 };
        } else {
          return { ...destino };
        }
      });

      return {
        ...state,
        data,
        loaded: true,
        loading: false,
        favorito: null,
      };
    }


    default:
      console.log('default');
      return state;
  }
}
