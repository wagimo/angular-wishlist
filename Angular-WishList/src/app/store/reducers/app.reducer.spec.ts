
import {
  initialState,
  reducer
} from './app.reducer' ;

import * as fromDestinosAction from '../actions/destino.action';
import { DestinosState } from './app.reducer';

describe('ReducerDestinoViajes', () => {

  let newStateLoading: DestinosState;
  const currentState = initialState;

  beforeEach(() => {
    const loadStateAction = new fromDestinosAction.LoadDestinoSuccess([
      {id: '1', nombre: 'd1', imagen: '', selected: false, servicios: [], votos: 0},
      {id: '2', nombre: 'd2', imagen: '', selected: false, servicios: [], votos: 0},
      {id: '3', nombre: 'd3', imagen: '', selected: true, servicios: [], votos: 1 }
    ]);
    newStateLoading = reducer(currentState, loadStateAction);
  });

  it('Cargando datos al state', () => {
    const action = new fromDestinosAction.LoadDestinoSuccess([
      {id: '1', nombre: 'd1', imagen: '', selected: false, servicios: [], votos: 0},
      {id: '2', nombre: 'd2', imagen: '', selected: false, servicios: [], votos: 0},
      {id: '3', nombre: 'd3', imagen: '', selected: true, servicios: [], votos: 1 }
    ]);
    const newState = reducer(currentState, action);
    expect(newState.data.length).toEqual(3);
    expect(newState.data[0].nombre).toEqual('d1');
  });

  it('Adicionando un nuevo item al state', () => {
    // adicionando un nuevo destino
    const actionNewDestino = new fromDestinosAction.AddDestinoSuccess(
      {id: '4', nombre: 'Cali', imagen: '', selected: false, servicios: [], votos: 0}
      );

    const newState1 = reducer(newStateLoading, actionNewDestino);
    expect(newState1.data.length).toEqual(4);
    expect(newState1.data[3].nombre).toEqual('Cali');
  });

  it('Eliminando un destino del state', () => {
    const eliminarDestinoAction = new fromDestinosAction.EliminarDestinoSuccess(
      '1'
    );

    const newState1 = reducer(newStateLoading, eliminarDestinoAction);
    expect(newState1.data.length).toEqual(2);
    expect(newState1.data[0].id).toEqual('2');
  });

  it('Actualizar destino', () => {

    const actionUpdateDestino = new fromDestinosAction.UpdateDestinoFavoritoSuccess(
      {id: '2', nombre: 'Cali', imagen: '123', selected: true, servicios: [], votos: 6}
      );

    const newState1 = reducer(newStateLoading, actionUpdateDestino);
    expect(newState1.data.length).toEqual(3);
    expect(newState1.data[1].nombre).toEqual('Cali');
    expect(newState1.data[1].selected).toEqual(true);
    expect(newState1.data[1].votos).toEqual(6);

  });
});
