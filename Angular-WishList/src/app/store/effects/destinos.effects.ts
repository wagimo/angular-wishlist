import { Injectable } from '@angular/core';
import { Observable, of, pipe } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as fromDestinoActions from '../actions/destino.action';
import { ApiDestinosServices } from '../../services/api-destinos-services.service';
import { ApiDestinosDexieService } from '../../services/destinos-dexie.service';




@Injectable({
  providedIn: 'root',
})
export class DestinosEffects {
  constructor(
    private actions$: Actions,
    private destinoServices: ApiDestinosServices,
    private destinosDexieServices: ApiDestinosDexieService
    ) {}

  /** FAVORITO EFECT: MARCA EL ULTIMO DESTINO ADICIONADO COMO FAVORITO   */
  @Effect()
  MarcarFavoritoeffects$: Observable<Action> = this.actions$.pipe(
    ofType(fromDestinoActions.ADD_DESTINO_SUCCESS),
    map(
        (action: fromDestinoActions.AddDestinoSuccess) =>
        new fromDestinoActions.UpdateDestinoFavoritoSuccess(action.payload)
    )
  );

  /** ADD DESTINO: EJECUTA EL SERVICIO Y ADICIONA EN EL BACKEND EL NUEVO DESTINO, AFECTANDO EL STORE DE LA APP. */
  @Effect()
  addDestino$: Observable<Action> = this.actions$.pipe(
      ofType(fromDestinoActions.ADD_DESTINO),
      // tap(x => console.log('fromDestinoActions.ADD_DESTINO ->', x)),
      map((action: fromDestinoActions.AddDestino) => action.payload),
      switchMap( payload => this.destinoServices.AddDestino(payload)
          .pipe(
              map(response => new fromDestinoActions.AddDestinoSuccess(response.oDestino)
          ),
          catchError(error => of(new fromDestinoActions.AddDestinoFail(error))
      ))
    )
  );

  /** LOAD DESTINOS: EJECUTA EL API PARA OBTENER TODOS LOS DESTINOS CREADOS. */
  @Effect()
  loadDestinos$: Observable<Action> = this.actions$.pipe(
      ofType(fromDestinoActions.LOAD_DESTINO),
      // tap(x => console.log('fromDestinoActions.LOAD_DESTINO ->', x)),
       switchMap( () =>  this.destinoServices.GetAllDestinos()   // OBTIENE LA INFORMACIÓN DESDE EL BACKEND
     // switchMap( () =>  this.destinosDexieServices.getList() // OBTIENE LA INFORMACIÓN DESDE INDEXEDBD
        .pipe(
            map(response => new fromDestinoActions.LoadDestinoSuccess(response)
        ),
      catchError(error => of(new fromDestinoActions.LoadDestinoFail(error))
    ))
  ));

  /** EJECUTA LA API PARA ELIMINAR EL DESTINO EN EL BACKEND Y POSTERIORMENTE EN EL STORE. */
  @Effect()
  removeDestino$: Observable<Action> = this.actions$.pipe(
      ofType(fromDestinoActions.ELIMINAR_DESTINO),
      // tap(x => console.log('fromDestinoActions.ELIMINAR_DESTINO ->', x)),
      map((action: fromDestinoActions.EliminarDestino) => action.payload),
      switchMap( payload => this.destinoServices.EliminarDestino(payload)
      .pipe(
          map(response => new fromDestinoActions.EliminarDestinoSuccess(response.id)
      ),
      catchError(error => of(new fromDestinoActions.EliminarDestinoFail(error))
    ))
  ));

    /** EJECUTA LA API PARA ACTUALIZAR EL DESTINO EN EL BACKEND Y POSTERIORMENTE EN EL STORE. */
    @Effect()
    updateDestino$: Observable<Action> = this.actions$.pipe(

        ofType(fromDestinoActions.VOTAR_DESTINO),
       // tap(x => console.log('fromDestinoActions.VOTAR_DESTINO ->', x)),
        map((action: fromDestinoActions.VotarDestino) => action.payload),
        switchMap( payload => this.destinoServices.actualizarDestino(payload)
        .pipe(
                map(response => new fromDestinoActions.VotarDestinoSuccess(response.destino))
              , catchError(error => of(new fromDestinoActions.UpdateDestinoFavoritoFail(error)))
            )
    ));

     /** EJECUTA LA API PARA ACTUALIZAR EL DESTINO COMO FAVORITO EN EL BACKEND Y POSTERIORMENTE EN EL STORE. */
     @Effect()
     updateDestinoFavorito$: Observable<Action> = this.actions$.pipe(

         ofType(fromDestinoActions.UPDATE_FAVORITO_DESTINO),
         map((action: fromDestinoActions.UpdateDestinoFavorito) => action.payload),
         switchMap( payload => this.destinoServices.actualizarDestinoFavorito(payload)
         .pipe(
                 map(response => new fromDestinoActions.UpdateDestinoFavoritoSuccess(response.destino))
               , catchError(error => of(new fromDestinoActions.UpdateDestinoFavoritoFail(error)))
             )
     ));

      /** EJECUTA LA API PARA RESETEAR LOS VOTOS DE TODOS LOS DESTINOS EN EL BACKEND Y POSTERIORMENTE EN EL STORE. */
      @Effect()
      resetVotos$: Observable<Action> = this.actions$.pipe(
          ofType(fromDestinoActions.RESET_VOTOS_DESTINO),
          switchMap( () => this.destinoServices.ResetVotos()
          .pipe(
                  map(response => new fromDestinoActions.ResetVotosDestinoSuccess(response.destino))
                , catchError(error => of(new fromDestinoActions.UpdateDestinoFavoritoFail(error)))
              )
      ));

       /** EJECUTA LA API PARA OBTENER EL DESTINO DESDE EL BACKEND */
       @Effect()
       getDestinoById$: Observable<Action> = this.actions$.pipe(
           ofType(fromDestinoActions.RESET_VOTOS_DESTINO),
           switchMap( () => this.destinoServices.ResetVotos()
           .pipe(
                   map(response => new fromDestinoActions.ResetVotosDestinoSuccess(response.destino))
                 , catchError(error => of(new fromDestinoActions.UpdateDestinoFavoritoFail(error)))
               )
       ));
}
