import { Action } from '@ngrx/store';
import { Destino } from '../../models/destino.interface';

export const LOAD_DESTINO = '[Destino] Cargando Destinos';
export const LOAD_DESTINO_SECCESS =  '[Destino] Cargando los destinos satisfactoriamente';
export const LOAD_FAILED = '[Destino] Error al cargar la información';

// ADD DESTINO
export const ADD_DESTINO         = '[Destino] add destino';
export const ADD_DESTINO_SUCCESS = '[Destino] add destino success';
export const ADD_DESTINO_FAIL    = '[Destino] add destino fail';

// MARCAR FAVORITO
export const UPDATE_FAVORITO_DESTINO = '[Destino] Invoca el dispatch parainiciar con la marcación del destino como favorito';
export const UPDATE_FAVORITO_DESTINO_SUCCESS = '[Destino] Marca el destino como favorito.';
export const UPDATE_FAVORITO_DESTINO_FAIL = '[Destino] Error al actualzar el estado del destino como Favorito.';

// ELIMINAR DESTINO
export const ELIMINAR_DESTINO = '[Destino] invoca el dispatch para iniciar con el proceso de eliminación del destino en el Backend y posteriormente en el store'
export const ELIMINAR_DESTINO_SUCCESS = '[Destino] Elimina el destino seleccionado';
export const ELIMINAR_DESTINO_FAIL = '[Destino] Error al elimina el destino seleccionado';

/** VOTAR DESTINO */
export const VOTAR_DESTINO_SUCCESS = '[Destino] Vota positiva o negativamente por el destino';
export const VOTAR_DESTINO =         '[Destino] Invoca el dispatch para lanzar el llamado al api ';

/** RESETEAR VOTOS */
export const RESET_VOTOS_DESTINO_SUCCESS = '[Destino] resetea los votos para todos los destino';
export const RESET_VOTOS_DESTINO = '[Destino] Invoca la acción para hacer dispatch sobre el servicio';

/** CONTAR TAG CLICK */
export const TAG_UP_COUNT = '[Destino] Cuenta el numero de veces que el usuario dió click sobre el control UP de votación';

export const TAG_DOWN_COUNT = '[Destino] Cuenta el numero de veces que el usuario dió click sobre el control Down de votación';



export class LoadDestino implements Action {
  readonly type = LOAD_DESTINO;
}

export class LoadDestinoSuccess implements Action {
  readonly type = LOAD_DESTINO_SECCESS;
  constructor(public payload: Destino[]) {}
}

export class LoadDestinoFail implements Action {
  readonly type = LOAD_FAILED;
  constructor(public payload: any) {}
}

// Add Destino
export class AddDestino implements Action{
  readonly type = ADD_DESTINO;

  constructor(public payload: Destino){}
}

export class AddDestinoSuccess implements Action{
readonly type = ADD_DESTINO_SUCCESS;

constructor(public payload: Destino){}
}

export class AddDestinoFail implements Action{
readonly type = ADD_DESTINO_FAIL;

constructor(public payload: any){}
}

// MARCAR FAVORITO

export class UpdateDestinoFavorito implements Action {
  readonly type = UPDATE_FAVORITO_DESTINO;
  constructor(public payload: Destino) { }
}

export class UpdateDestinoFavoritoSuccess implements Action {
  readonly type = UPDATE_FAVORITO_DESTINO_SUCCESS;
  constructor(public payload: Destino) { }
}

export class UpdateDestinoFavoritoFail implements Action {
  readonly type = UPDATE_FAVORITO_DESTINO_FAIL;
  constructor(public payload: string) { }
}

/**  ELIMINAR DESTINO */

export class EliminarDestino implements Action {
  readonly type = ELIMINAR_DESTINO;
  constructor(public payload: Destino) { }
}
export class EliminarDestinoSuccess implements Action {
  readonly type = ELIMINAR_DESTINO_SUCCESS;
  constructor(public payload: string) { }
}

export class EliminarDestinoFail implements Action {
  readonly type = ELIMINAR_DESTINO_FAIL;
  constructor(public payload: string) { }
}

/** VOTAR DESTINO */
export class VotarDestino implements Action {
  readonly type = VOTAR_DESTINO;
  constructor(public payload: Destino) { }
}

export class VotarDestinoSuccess implements Action {
  readonly type = VOTAR_DESTINO_SUCCESS;
  constructor(public payload: Destino) { }
}

/** RESETEAR VOTOS */
export class ResetVotosDestino implements Action {
  readonly type = RESET_VOTOS_DESTINO;
}

export class ResetVotosDestinoSuccess implements Action {
  readonly type = RESET_VOTOS_DESTINO_SUCCESS;
  constructor(public payload: any) { }
}


export type DestinosActions =
LoadDestino
| LoadDestinoSuccess
| LoadDestinoFail
| AddDestino
| AddDestinoSuccess
| AddDestinoFail
| UpdateDestinoFavorito
| UpdateDestinoFavoritoSuccess
| UpdateDestinoFavoritoFail
| EliminarDestino
| EliminarDestinoSuccess
| EliminarDestinoFail
| VotarDestino
| VotarDestinoSuccess
| ResetVotosDestino
| ResetVotosDestinoSuccess;
