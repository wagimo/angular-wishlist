import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';

import { SecurityModule } from './security/security.module';
import { ComponentModule } from './components/component.module';
import { InjectionToken } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';

import { TranslationLoader } from './translation/translationLoader';
import { ApiDestinosDexieService } from './services/destinos-dexie.service';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
// import { TackClickDirective } from './tack-click.directive';
// import { EspiaDirective } from './espia.directive';



export interface BackEndConfig {
  url: string;
}

export const URL_CONFIG_VALUE: BackEndConfig = {
  url: 'http://localhost:3001',
};

export const APP_CONFIG_BACKEND = new InjectionToken<BackEndConfig>(
  'backEnd.search.config'
);

export function HttpLoaderFactory(http: HttpClient, apidestinos: ApiDestinosDexieService): any {
  return new TranslationLoader(http, apidestinos);
}

export function CreateTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}





@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatInputModule,
    SecurityModule,
    ComponentModule,
    RouterModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: CreateTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  exports: [MatFormFieldModule, MatAutocompleteModule, MatInputModule],
  bootstrap: [AppComponent],
  providers: [ApiDestinosDexieService, { provide: APP_CONFIG_BACKEND, useValue: URL_CONFIG_VALUE }]
  // providers:[
  //   { provide: APP_CONFIG_BACKEND, useValue: URL_CONFIG_VALUE }
  // ]
})
export class AppModule {}
