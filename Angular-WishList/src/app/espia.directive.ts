import { Directive, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appEspia]'
})
export class EspiaDirective implements OnInit, OnDestroy {

static contador = 0;
log = ( msg: string ) =>  console.log(` Numero de Evento: ${EspiaDirective.contador++} ${msg}`) ;

ngOnDestroy(): void{
  this.log('*** ngOnDestroy ***');
}

ngOnInit(): void{
  this.log('*** ngOnInit ***');
}

}
