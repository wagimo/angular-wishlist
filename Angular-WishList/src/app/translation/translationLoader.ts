import { TranslateLoader } from '@ngx-translate/core';
import { from, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { flatMap } from 'rxjs/operators';
import { ApiDestinosDexieService } from '../services/destinos-dexie.service';

const base_url = environment.base_url;

export class TranslationLoader implements TranslateLoader {
    constructor(private http: HttpClient, private apidestinos: ApiDestinosDexieService) {}

  getTranslation(lang: string): Observable<any> {
// debugger;
    const translation = this.apidestinos.getTranslationListByFilter(lang)
                        .then(res => {
                          if (res.length === 0 )
                          {
                            this.http
                                .get(`${base_url}/api/translation/${lang}`)
                                .toPromise()
                                .then((resultApi: any) => {
                                  const { traducciones  } = resultApi;
                                  this.apidestinos.seTranslations(traducciones);
                                  return resultApi;
                                });
                          }
                          return res;
                        })
                        .then( traduccion => {
                          console.log("Traduccion",traduccion);
                          return traduccion;
                        })
                        .then( traduccion => {
                          return traduccion.map((t: any) => ({ [t.key]: t.value }));
                        })
                        .catch(err => console.log('Error', err));


    return from(translation)
           .pipe( flatMap ((e: any) => from(e)));

  }


}
