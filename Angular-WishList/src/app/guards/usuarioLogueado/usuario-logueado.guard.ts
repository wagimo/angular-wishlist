import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  CanLoad,
  Route,
  UrlSegment,
  UrlTree,
} from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UsuarioLogueadoGuard implements CanActivate, CanLoad {
  constructor(private usuarioServices: AuthService, private router: Router) {}
  canLoad(route: Route, segments: UrlSegment[]): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.usuarioServices.validarToken().pipe(
      tap((isAutenticated) => {
        console.log(isAutenticated);
        if (!isAutenticated) {
          this.router.navigateByUrl('/login');
        }
      })
    );
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    return this.usuarioServices.validarToken().pipe(
      tap((isAutenticated) => {
        if (!isAutenticated) {
          this.router.navigateByUrl('/login');
        }
      })
    );
  }
}
