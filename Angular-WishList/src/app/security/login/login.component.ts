import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls  : ['./login.component.css' ]
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private usuarioServices: AuthService
  ) { }

  public loginForm = this.fb.group({
    email: [ localStorage.getItem('email') || '' , [Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'), Validators.required]],
    password: ['', [ Validators.required, Validators.minLength(6)]],
    remember: [localStorage.getItem('email') ? true : false]
  });

  ngOnInit(): void {

  }



  login(): void{


    this.usuarioServices.login( this.loginForm.value )
    .subscribe( subs => {
      console.log(subs);
      if ( this.loginForm.get('remember').value )
        {
          localStorage.setItem('email', this.loginForm.get('email').value);
        }
        else{
          localStorage.removeItem('email');
        }
      this.router.navigateByUrl('/home/destinos');
    },
      err => {
        console.log(err);
      }
      );
  }

}
