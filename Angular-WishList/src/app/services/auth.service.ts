import { Injectable } from '@angular/core';
import { observable, Observable } from 'rxjs';
import shortid from 'shortid';
import { ILogin } from '../models/login.interface';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }


  validarToken(): Observable<boolean> {

    const token = localStorage.getItem('token') || '';
    const result = token !== '';
    const obs$ = new Observable<boolean>(s => s.next(result));
    return obs$;
  }

  login(data: ILogin): Observable<any>{
    const tokenTemp =  shortid.generate();
    localStorage.setItem('token', tokenTemp );
    const obs$ = new Observable<boolean>(s => s.next(tokenTemp));
    return obs$;
  }



}


