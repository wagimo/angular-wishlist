

import { HttpClient } from '@angular/common/http';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
// import { APP_CONFIG_BACKEND, BackEndConfig } from '../app.module';


import { environment } from '../../environments/environment';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root',
})
export class ApiDestinosServices {

  vuelos = [
    {
      id: 1,
      pais: 'Colombia',
      ciudad: 'Bogota',
      tiempo: '2h',
      valor: '200 usd'
    }, {
      id: 2,
      pais: 'Colombia',
      ciudad: 'Medellin',
      tiempo: '2h',
      valor: '200 usd'
    },{
      id: 3,
      pais: 'Colombia',
      ciudad: 'Cali',
      tiempo: '2h',
      valor: '200 usd'
    },{
      id: 4,
      pais: 'Colombia',
      ciudad: 'Barranquilla',
      tiempo: '3h',
      valor: '210 usd'
    },{
      id: 5,
      pais: 'Colombia',
      ciudad: 'Cartagena',
      tiempo: '3h',
      valor: '210 usd'
    },{
      id: 6,
      pais: 'Colombia',
      ciudad: 'Santa Marta',
      tiempo: '4h',
      valor: '250 usd'
    },{
      id: 7,
      pais: 'Colombia',
      ciudad: 'San Andres',
      tiempo: '5h',
      valor: '350 usd'
    },{
      id: 8,
      pais: 'Mexico',
      ciudad: 'Cancún',
      tiempo: '6h',
      valor: '500 usd'
    },{
      id: 9,
      pais: 'Mexico',
      ciudad: 'Guadalajara',
      tiempo: '7h',
      valor: '500 usd'
    },{
      id: 10,
      pais: 'Mexico',
      ciudad: 'Acapulco',
      tiempo: '7h',
      valor: '500 usd'
    },{
      id: 11,
      pais: 'Brazil',
      ciudad: 'Rio',
      tiempo: '9h',
      valor: '700 usd'
    },{
      id: 12,
      pais: 'Brazil',
      ciudad: 'Brasilia',
      tiempo: '9h',
      valor: '700 usd'
    },
  ];
  destinos: any[] = [];

  constructor(
      private http: HttpClient,
      // @Inject(forwardRef(() => APP_CONFIG_BACKEND )) private appConfig: BackEndConfig
      ) {

  }

  GetVuelos(): Observable<any[]>{

    const obs$ = new Observable<any[]>( obs =>
      obs.next(this.vuelos)
    );
    return obs$;
  }

  GetVuelosById(id: number): Observable<any>{

    const vuelo = this.vuelos.find( x => x.id == id);
    const obs$ = new Observable<any>( obs =>
      obs.next(vuelo)
    );
    return obs$;
  }

  GetAllCountries(): Observable<any> {
    const newUrl = `${base_url}/api/ciudades`;
    return this.http.get(newUrl);
  }


  GetAllDestinos(): Observable<any>{
    const newUrl = `${base_url}/api/destinos`;
    return this.http.get<any>(newUrl).pipe(
      map( res => res.destinos )
    );
  }

  GetDestinoById(id: string): Observable<any>{
    const newUrl = `${base_url}/api/destinos/searchId/${id}`;
    return this.http.get(newUrl);
  }

  AddDestino(destino: any): Observable<any>{
    const newUrl = `${base_url}/api/destinos`;
    return this.http.post(newUrl, destino).pipe(
      tap(  (resp: any) => {
          //  console.log('Response Create->', resp.oDestino);
      })
    );
  }

  EliminarDestino(destino: any): Observable<any>{
    const newUrl = `${base_url}/api/destinos/${destino.id}`;
    return this.http.delete(newUrl).pipe(
      tap(  (resp: any) => {
           // console.log('Response Delete->', resp.id);
      })
    );
  }

  actualizarDestino(destino: any): Observable<any>{
    const newUrl = `${base_url}/api/destinos/${destino.id}`;
    return this.http.put(newUrl, destino).pipe(
      tap(  (resp: any) => {
          //  console.log('Response Update->', resp.destino);
      })
    );
  }

  actualizarDestinoFavorito(destino: any): Observable<any>{
    const newUrl = `${base_url}/api/destinos/favorito/${destino.id}`;
    return this.http.put(newUrl, destino).pipe(
      tap(  (resp: any) => {
           // console.log('Response Update->', resp.destino);
      })
    );
  }

  ResetVotos(): Observable<any> {
    const newUrl = `${base_url}/api/destinos/reset`;
    return this.http.get(newUrl);
  }

}
