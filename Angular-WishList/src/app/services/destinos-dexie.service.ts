import { Injectable } from '@angular/core';
import Dexie from 'dexie';
import { Destino } from '../models/destino.interface';
import { from, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiDestinosDexieService {

  private db: any;
  private DestinosTable: Dexie.Table<Destino> = null;
  private TranslationTable: Dexie.Table<any> = null;

  constructor() {
    this.initializeIndexedDb();
   }


  initializeIndexedDb(): void{

    this.db = new Dexie('Destinos-db');
    this.db.version(1).stores({
      destinos: 'id, nombre, imagen, selected, servicios, votos, precio'
    });

    this.db.version(2).stores({
      destinos: 'id, nombre, imagen, selected, servicios, votos, precio',
      translations: '++id, lang, key, value'
    });

    this.DestinosTable = this.db.table('destinos');
    this.TranslationTable = this.db.table('translations');
  }

  async setDestinoIndexedDB(destino: Destino): Promise<any> {
    try {
      await this.DestinosTable.add(destino);
    } catch (error) {
      console.log('error');
    }
  }

  clearRowsTableDestino(): void {
    this.db.DestinosTable.clear();
  }

  async getDestinoIndexedDBById(id): Promise<any> {
    return await this.db.DestinosTable.get(id);
  }

  async getDestinosList(): Promise<any> {
    return  this.DestinosTable.toArray();
  }

  getList(): Observable<any>{
    return from(this.getDestinosList());
  }


  async seTranslations(translation: any): Promise<any> {
    try {
      await this.TranslationTable.add(translation);
    } catch (error) {
      console.log('error', error);
    }
  }

  async getTranslationList(): Promise<any> {
    return  this.TranslationTable.toArray();
  }

  async getTranslationListByFilter(lang: string): Promise<any> {
    return  this.TranslationTable
            .where('lang')
            .equals(lang)
            .toArray();
  }



  getTranslationobservableList(): Observable<any>{
    return from(this.getTranslationList());
  }

}
