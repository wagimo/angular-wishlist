import { Directive, ElementRef, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { fromEvent } from 'rxjs';
import { AppState } from './store';
import * as fromStore from './store';
import { Destino } from './models/destino.interface';



@Directive({
  selector: '[appTackClick]'
})
export class TackClickDirective {

  private element: HTMLInputElement;
  private oDestino: Destino;

  constructor(private elementhtml: ElementRef, private store: Store<AppState>) {
    this.element = elementhtml.nativeElement;
    fromEvent(this.element, 'click')
    .subscribe( e => this.trackEvent(e));
   }

   @Input()
   set appTackClick(destino: Destino)
   {
      this.oDestino = destino;
   }

   trackEvent(e: Event): void{
      const tags = this.element.attributes.getNamedItem('data-track').value;
      let newDestino: Destino;

      if ( tags === 'tag1_up'){
        const clicks = this.oDestino.trackClicUp + 1;
        const votos =  this.oDestino.votos + 1;
        newDestino = { ...this.oDestino, trackClicUp: clicks, votos };
        // this.store.dispatch(new fromStore.TagUpCountClick(newDestino));
      }
      else{
        const clicks = this.oDestino.trackClicDown + 1;
        const votos =  this.oDestino.votos - 1;
        newDestino = { ...this.oDestino, trackClicDown: clicks, votos };
        // this.store.dispatch(new fromStore.TagDownCountClick(newDestino));
      }
      this.store.dispatch(new fromStore.VotarDestino(newDestino));

      console.log('||||||||--------->>>>  ClickUp', newDestino.trackClicUp);
      console.log('||||||||--------->>>>  ClickDown', newDestino.trackClicDown);
   }
}
