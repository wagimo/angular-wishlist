import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-component',
  templateUrl: './component.component.html',
  styles: [
  ]
})
export class ComponentComponent implements OnInit {

  constructor(
    public translateServices: TranslateService,
    private router: Router ) {

      this.translateServices.setDefaultLang('es');
      this.translateServices.use('es');

      // translate.getTranslation('en').subscribe(
      //    x => console.log('Traduccion ->', x),
      //    error => console.log('Error', error),
      //    () => console.log('completado')
      // );
      // translate.setDefaultLang('es');
    }

  ngOnInit(): void {
  }

  salir(): void{
    localStorage.removeItem('token');
    this.router.navigateByUrl('/login');
  }

  selectedValue( lang: string): void
  {
    this.translateServices.use(lang);
  }
}
