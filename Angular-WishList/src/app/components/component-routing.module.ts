import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentComponent } from './component.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { UsuarioLogueadoGuard } from '../guards/usuarioLogueado/usuario-logueado.guard';
import { DestinosvisitadosComponent } from './destinosvisitados/destinosvisitados.component';


const routes: Routes = [

  {
    path: 'home',
    component: ComponentComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    // canLoad:[ UsuarioLogueadoGuard ],
    children: [
      {
        path: '',
        redirectTo: 'destinos',
        pathMatch: 'full'
      },
      {
        path: 'destinos',
        component: ListaDestinosComponent
      },
      {
        path: 'visitados',
        component: DestinosvisitadosComponent
      },
      {
        path: 'detalle/:id',
        component: DestinoDetalleComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentRoutingModule { }
