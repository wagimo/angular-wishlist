import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  HostBinding,
  ViewChild,
} from '@angular/core';

import {
  FormBuilder,
  FormControl,
  ValidatorFn,
  Validators,
} from '@angular/forms';

import { Destino } from '../../models/destino.interface';
import { AutocompleteComponent } from '../autocomplete/autocomplete.component';
import shortid from 'shortid';

@Component({
  selector: 'app-formsdestinoviaje',
  templateUrl: './formsdestinoviaje.component.html',
  styles: [],
})
export class FormsdestinoviajeComponent {
  @Output() agregarDestino: EventEmitter<Destino>;
  @Output() resetearVotos: EventEmitter<any>;

  minLengtParam = 3;
  public formSubmitted = false;
  @ViewChild('autocompleteComponent') autocompleteComponent: AutocompleteComponent;

  @HostBinding('attr.class') cssClass = 'w-100';
  constructor(private fb: FormBuilder) {
    this.agregarDestino = new EventEmitter();
    this.resetearVotos = new EventEmitter();
  }

  public registerForm = this.fb.group({
    id: '',
    nombre: ['', Validators.required],
    imagen: [
      '',
      Validators.compose([
        Validators.required,
        this.minLengthParametrizable(this.minLengtParam),
      ]),
    ],
    selected: false,
    votos: 0
  });

  GuardarDestino(): void {
    this.formSubmitted = true;
    if (this.registerForm.invalid) {
      return;
    }

    const nuevoDestino: Destino = {
      id: shortid.generate(),
      nombre: this.registerForm.value.nombre,
      imagen: this.registerForm.value.imagen,
      selected: true,
      servicios: [
        {nombre: 'wifi'},
        {nombre: 'Parqueadero'},
        {nombre: 'Restaurante'},
        {nombre: 'Bar'},
        {nombre: 'Piscina'},
        {nombre: 'Jacuzzi'}
      ],
      votos: 0,
      precio: Math.floor( Math.random() * 9 ) + 1,
      trackClicUp: 0,
      trackClicDown: 0
    };
    this.agregarDestino.emit(nuevoDestino);
    this.resetForm();
    this.autocompleteComponent.clearList();
  }



  resetForm(): void {
    this.registerForm.reset({
      nombre: '',
      imagen: '',
      selected: false,
      servicios: []
    });
    this.formSubmitted = false;
  }

  campoNoValido(nombre: string): boolean {
    if (this.registerForm.get(nombre).invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }

  minLength(control: FormControl): { [key: string]: boolean } {
    const longitud = control.value.toString().trim().length;

    if (longitud > 0 && longitud < 5) {
      return { minLength: true };
    }
    return null;
  }

  minLengthParametrizable(min: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
      const longitud = control.value.toString().trim().length;
      if (longitud > 0 && longitud < min) {
        return { minLength: true };
      }
      return null;
    };
  }

  setDestino(destino: string): void {
    this.registerForm.setValue({
      ...this.registerForm.value,
      nombre: destino,
    });
  }

  ResetVote(): void{
    this.resetearVotos.emit('reset');
  }


}
