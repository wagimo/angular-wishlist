import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map',
  template: `
          <mgl-map
          [style]="style"
          [zoom]="[1]">
            <mgl-marker [lngLat]="[-75.551012, 10.421185]">
              <div class="marker" onclick="alert('hola')">Open</div>
            </mgl-marker>
        </mgl-map>
  `,
 styles: [`
     mgl-map {
       height: 50vh;
       width: 100vw;
     }
`]
})
export class MapComponent  {

  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  }

}
