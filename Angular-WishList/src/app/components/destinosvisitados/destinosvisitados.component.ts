import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from '../../store';
import { AppState } from '../../store/reducers/index';

@Component({
  selector: 'app-destinosvisitados',
  templateUrl: './destinosvisitados.component.html',
  styles: [
  ]
})
export class DestinosvisitadosComponent implements OnInit {

  destinos = [];

  constructor(private store: Store<AppState>) {
    store.select('destinos').subscribe(response => {
      this.destinos = response.data;
    });
  }

  ngOnInit(): void {
    this.store.dispatch(new fromStore.LoadDestino());
  }

}
