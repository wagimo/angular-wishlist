import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ComponentRoutingModule } from './component-routing.module';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormsdestinoviajeComponent } from './formsdestinoviaje/formsdestinoviaje.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { reducer, effects  } from '../store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { ComponentComponent } from './component.component';
import { DestinosvisitadosComponent } from './destinosvisitados/destinosvisitados.component';
import { ApiDestinosServices } from '../services/api-destinos-services.service';
import { MapComponent } from './map/map.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EspiaDirective } from '../espia.directive';
import { TackClickDirective } from '../tack-click.directive';






@NgModule({
  declarations: [
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormsdestinoviajeComponent,
    AutocompleteComponent,
    ComponentComponent,
    DestinosvisitadosComponent,
    MapComponent,
    EspiaDirective,
    TackClickDirective
  ],
  imports: [
    ComponentRoutingModule,
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxMapboxGLModule,
    StoreModule.forRoot( reducer ),
    StoreDevtoolsModule.instrument({
    }),
    EffectsModule.forRoot(effects),
    BrowserAnimationsModule
  ],
  exports:[
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormsdestinoviajeComponent,
    AutocompleteComponent,
    ComponentComponent,
    DestinosvisitadosComponent

  ],
  providers: [
    ApiDestinosServices
  ],
})
export class ComponentModule { }
