import {
  Component,
  OnInit,
  Input,
  HostBinding,
  EventEmitter,
  Output,
} from '@angular/core';
import { Destino } from '../../models/destino.interface';
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  animations: [
    trigger('esfavorito', [  // nombre de la animacion
      state(
        'estadoFavorito',
        style({
          backgroundColor: '#D3EB96',
        })
      ),
      state(
        'estadoNoFavorito',
        style({
          backgroundColor: '#F5F2EE',
        })
      ),
      transition('estadoNoFavorito => estadoFavorito', [animate('5s')]),
      transition('estadoFavorito => estadoNoFavorito', [animate('1s')])
    ]),
  ]
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: Destino;
  @Input('idx') index: number;

  @Output() destinoSelected: EventEmitter<Destino>;
  @Output() eliminarSelected: EventEmitter<Destino>;
  @Output() votarSelected: EventEmitter<Destino>;

  @HostBinding('attr.class') cssClass = 'col-md-4 mt-2';
  constructor() {
    this.destinoSelected = new EventEmitter();
    this.eliminarSelected = new EventEmitter();
    this.votarSelected = new EventEmitter();
  }

  ngOnInit(): void {}

  Saludar(val: HTMLInputElement): void {
    console.log(val.value);
  }

  MarcarFavorito($event): void {
    $event.preventDefault();
    if (!this.destino.selected) {
      const oDestino = { ...this.destino, selected: true };
      this.destinoSelected.emit(oDestino);
    }
  }

  EliminarDestino($event): void {
    $event.preventDefault();
    this.eliminarSelected.emit(this.destino);
  }

  // VoteUp($event): void {
  //   $event.preventDefault();
  //   const votos = this.destino.votos + 1;
  //   const oDestino: Destino = { ...this.destino, votos };
  //   this.votarSelected.emit(oDestino);
  // }

  // VoteDown($event): void {
  //   $event.preventDefault();
  //   const votos = this.destino.votos - 1;
  //   const oDestino: Destino = { ...this.destino, votos };
  //   this.votarSelected.emit(oDestino);
  // }
}
