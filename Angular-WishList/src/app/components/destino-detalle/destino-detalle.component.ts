import { Component, OnInit } from '@angular/core';
import { ApiDestinosServices } from '../../services/api-destinos-services.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html'
})
export class DestinoDetalleComponent implements OnInit {
  destinoId: any;
  destino: any = {};

  // style = {
  //   sources: {
  //     world: {
  //       type: 'geojson',
  //       data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json',
  //     },
  //   },
  //   version: 8,
  //   layers: [
  //     {
  //       id: 'countries',
  //       type: 'fill',
  //       source: 'world',
  //       layout: {},
  //       paint: {
  //         'fill-color': '#6F788A'
  //       },
  //     },
  //   ],
  // };

  constructor(
    private services: ApiDestinosServices,
    private rutaActiva: ActivatedRoute

  ) { }

  ngOnInit(): void {

    this.rutaActiva.params.subscribe( params => {
      this.destinoId = params.id;
    });


    if (this.destinoId !== undefined){

      this.services.GetDestinoById(this.destinoId).subscribe( response => {
        this.destino = response.destino;
      });
    }

  }

}
