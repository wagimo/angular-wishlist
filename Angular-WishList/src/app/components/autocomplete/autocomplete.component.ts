import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  Input,
  forwardRef,
} from '@angular/core';
import {
  Subscription,
  Observable,
  fromEvent,
  BehaviorSubject,
  Subject,
} from 'rxjs';
import { ApiDestinosServices } from '../../services/api-destinos-services.service';
import { FormControl } from '@angular/forms';
import {
  map,
  debounceTime,
  filter,
  distinctUntilChanged,
  switchMap,
} from 'rxjs/operators';
import { ControlContainer, FormGroupDirective } from '@angular/forms';
// import { Inject } from '@angular/core';
// import { BackEndConfig, APP_CONFIG_BACKEND } from '../component.module';
import { tap } from 'rxjs/operators';





@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  viewProviders: [
    {
      provide: ControlContainer,
      useExisting: FormGroupDirective,
    },
  ]
})
export class AutocompleteComponent implements OnInit, OnDestroy {
  subject$: Subject<string> = new BehaviorSubject<string>('');
  @Output() destinoSelected: EventEmitter<string>;
  @Input() controlName: string;
  searchResults: any[] = [];

  options: string[] = [];
  dataArray: any;
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  countries: any[];
  private subs = new Subscription();

  constructor(
    private destinoServices: ApiDestinosServices
    ) {
    this.destinoSelected = new EventEmitter();
  }

  ngOnInit(): void {
    const input = document.querySelector('#nombre');
    fromEvent(input, 'input')
      .pipe(
        map((e: KeyboardEvent) => {
          const val = (e.target as HTMLInputElement).value;
          if (val.length === 0) {
            this.searchResults = [];
            return '';
          }
          return val;
        }),
        filter((value) => value.trim().length >= 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap( val => {
              return this.filterData(val);
        })
      )
      .subscribe((result) => {

        this.searchResults = result;
      });
  }

  filterData(val: string): Observable<string[]> {

    return this.destinoServices.GetAllCountries()
    .pipe(

      map( (response: any) => {
        return response.ciudades.filter( option => {

          return option.ciudad.toLowerCase().indexOf(val.toLowerCase()) === 0;
        });
      }
      )
    );
  }
  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter((op) => op.toLowerCase().includes(filterValue));
  }

  ngOnDestroy(): void {
    if (this.subs) {
      this.subs.unsubscribe();
    }
  }

  clearList(): void {
    this.searchResults = [];
  }
}
