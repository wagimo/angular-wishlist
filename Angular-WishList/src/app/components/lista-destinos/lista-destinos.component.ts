import { Component, OnDestroy, OnInit } from '@angular/core';
import { Destino } from '../../models/destino.interface';
import { Subscription } from 'rxjs';
import { ApiDestinosServices } from '../../services/api-destinos-services.service';
import { Store } from '@ngrx/store';
import * as fromStore from '../../store';
import { AppState } from '../../store/reducers/index';
import { ApiDestinosDexieService } from '../../services/destinos-dexie.service';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html'
})
export class ListaDestinosComponent implements OnInit, OnDestroy {

  destinos: Destino[];
  log: Destino[] = [];
  subscription: Subscription;

  constructor(
    public destinoServices: ApiDestinosServices,
    private store: Store<AppState> ,
    private destinosDexieServices: ApiDestinosDexieService
    ) {
      this.subscription = store.select('destinos').subscribe(response => {
      this.destinos = response.data;
      if ( response.favorito )
      {
        this.log.push(response.favorito);
      }
    });
  }

  ngOnInit(): void {
    this.LoadDestinos();
  }

  ngOnDestroy(): void{
     this.subscription.unsubscribe();
  }

  AddDestino(destino: Destino): void {
    this.store.dispatch(new fromStore.AddDestino(destino));
    this.destinosDexieServices.setDestinoIndexedDB(destino);
  }

  DestinoSeleccionado(destino: Destino): void {
    this.store.dispatch( new fromStore.UpdateDestinoFavorito(destino));
  }

  LoadDestinos(): void{
    this.store.dispatch(new fromStore.LoadDestino());
  }

  EliminarDestino(destino: Destino): void{
    this.store.dispatch(new fromStore.EliminarDestino(destino));
  }

  VotarDestino(destino: Destino): void{
   // this.store.dispatch(new fromStore.VotarDestino(destino));
  }

  ResetVote(reset: string): void{
    if (this.destinos.length > 0)
    {
      this.store.dispatch(new fromStore.ResetVotosDestino());
    }

  }
}
