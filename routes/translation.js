
const { Router } = require('express');
const { getAll } = require('../controllers/translation');
const route = new Router();

//GET
route.get('/:lang',getAll );

module.exports = route;