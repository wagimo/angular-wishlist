const { Router } = require('express');
const { 
	getAllDestinos, 
	crearDestino, 
	getByCity, 
	getById, 
	eliminarDestino,
	actualizarDestino,
	resetVotos,
	actualizarDestinoFavorito
 } = require('../controllers/destinos');
const route = new Router();

// GET
route.get('/',getAllDestinos );

route.post('/',crearDestino)

route.get('/search/:param',getByCity)

route.get('/searchId/:param',getById)

route.delete('/:id', eliminarDestino)

route.put('/:id', actualizarDestino)

route.put('/favorito/:id', actualizarDestinoFavorito)

route.get('/reset',resetVotos)

module.exports = route;