const { Router } = require('express');
const { getAll } = require('../controllers/ciudades');
const route = new Router();

// GET
route.get('/',getAll );

module.exports = route;