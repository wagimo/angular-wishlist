

const { response } = require('express');
const DestinoModel = require('../models/destinos');

const getAllDestinos = async(req, resp = response) => {
	try{
		const destinos = DestinoModel.AllDestino();
		return resp.status(200).json({
			ok:true,
			destinos
		});
	}
	catch( err)
	{
		return resp.status(500).json({
            ok: true,
            msg: 'Error al retornar la lista de destinos!'
        });
	}
};

const crearDestino = async(req, resp = response) =>{


	try{
		
		const oDestino = {...req.body};		
		DestinoModel.AddDestino(oDestino)

		return resp.status(200).json({
			ok:true,
			oDestino
		});
	}
	catch( err)
	{
		return resp.status(500).json({
            ok: true,
            msg: 'Error al crear el destino!' + err
        });
	}
}

const getByCity = async(req, resp = response) => {
	try{
				
		const destinos = DestinoModel.SearchByName(req.params.param);
		return resp.status(200).json({
			ok:true,
			destinos
		});
	}
	catch( err)
	{
		return resp.status(500).json({
            ok: true,
            msg: 'Error al retornar la lista de destinos!'
        });
	}
};

const getById = async(req, resp = response) => {
	try{
			
		const destino = DestinoModel.SearchById(req.params.param);
		
		return resp.status(200).json({
			ok:true,
			destino
		});
	}
	catch( err)
	{
		return resp.status(500).json({
            ok: true,
            msg: 'Error al retornar el destino por ID!'
        });
	}
};

const eliminarDestino = async(req, resp = response) =>{


	try{
		
		const id = req.params.id;			
		DestinoModel.RemoveDestino(id)

		return resp.status(200).json({
			ok:true,
			id
		});
	}
	catch( err)
	{
		return resp.status(500).json({
            ok: true,
            msg: 'Error al eliminar el destino!' + err
        });
	}
}

const actualizarDestino = async(req, resp = response) =>{


	try{
	
		const id = req.params.id;		
		const destino = DestinoModel.SearchById(id);
		if(!destino)
		{
			return resp.status(404).json({
                ok: false,
                msg: 'Destino no encontrado para actualización'
            });
		}
		
		const nuevoDestino = { ...destino, ...req.body };
		DestinoModel.UpdateDestino(nuevoDestino)
		return resp.status(200).json({
			ok:true,
			destino: nuevoDestino
		});
	}
	catch( err)
	{
		return resp.status(500).json({
            ok: true,
            msg: 'Error al actualizar el destino!' + err
        });
	}
}

const actualizarDestinoFavorito = async(req, resp = response) =>{


	try{
	
		
		const id = req.params.id;		
		const destino = DestinoModel.SearchById(id);
		if(!destino)
		{
			return resp.status(404).json({
                ok: false,
                msg: 'Destino no encontrado para actualización'
            });
		}
		
		const nuevoDestino = { ...destino, ...req.body };		
		DestinoModel.marcarFavorito(nuevoDestino)
		return resp.status(200).json({
			ok:true,
			destino: nuevoDestino
		});
	}
	catch( err)
	{
		return resp.status(500).json({
            ok: true,
            msg: 'Error al actualizar el destino!' + err
        });
	}
}

const resetVotos = async(req, resp = response) =>{


	try{
		DestinoModel.ResetVotos()
		return resp.status(200).json({
			ok:true			
		});
	}
	catch( err)
	{
		return resp.status(500).json({
            ok: true,
            msg: 'Error al actualizar el destino!' + err
        });
	}
}


module.exports = {
	getAllDestinos,
	crearDestino,
	getByCity,
	getById,
	eliminarDestino,
	actualizarDestino,
	resetVotos,
	actualizarDestinoFavorito
}
