
const { response } = require('express');
const CiudadesModel = require('../models/ciudades');

const getAll = async(req, resp = response) => {
	try{
		const ciudades = CiudadesModel.GetAll();
		
		return resp.status(200).json({
			ok:true,
			ciudades
		});
	}
	catch( err)
	{
		return resp.status(500).json({
            ok: true,
            msg: 'Error al retornar la lista de ciudades!'
        });
	}
};


module.exports = {
	getAll
}
