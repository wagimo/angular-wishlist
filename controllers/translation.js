const { response } = require('express');
const TranslationModel = require('../models/translation');

const getAll = async(req, resp = response) => {
	try{
		
		const lang = req.params.lang;
		const traducciones =  TranslationModel.AllTranslations(lang);
		console.log(traducciones);
		return resp.status(200).json({
			ok:true,
			traducciones
		});
	}
	catch( err)
	{
		return resp.status(500).json({
            ok: true,
            msg: 'Error al retornar la lista de traducciones!'
        });
	}
};


module.exports = {
	getAll
}