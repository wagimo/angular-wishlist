

let ciudadesList = [
	{
		id: 1,
		pais: 'España',
		ciudad: 'Madrid'
	  },
	  {
		id: 2,
		pais: 'España',
		ciudad: 'Barcelona'
	  },
	  {
		id: 3,
		pais: 'España',
		ciudad: 'Alicante'
	  },
	  {
		id: 4,
		pais: 'Italia',
		ciudad: 'Roma'
	  },
	  {
		id: 5,
		pais: 'Italia',
		ciudad: 'Venecia'
	  },
	  {
		id: 6,
		pais: 'Italia',
		ciudad: 'Milán'
	  },
	  {
		id: 7,
		pais: 'Francia',
		ciudad: 'París'
	  },
	  {
		id: 8,
		pais: 'Francia',
		ciudad: 'Niza'
	  },
	  {
		id: 9,
		pais: 'Francia',
		ciudad: 'Marsella'
	  },
	  {
		id: 10,
		pais: 'Colombia',
		ciudad: 'Bogotá'
	  },
	  {
		id: 11,
		pais: 'Colombia',
		ciudad: 'Medellin'
	  },
	  {
		id: 12,
		pais: 'Colombia',
		ciudad: 'Cartagena'
	  },
	  {
		id: 13,
		pais: 'Colombia',
		ciudad: 'Barranquilla'
	  },
	  {
		id: 14,
		pais: 'Colombia',
		ciudad: 'Cali'
	  },
	  {
		id: 15,
		pais: 'Colombia',
		ciudad: 'Bucaramanga'
	  },
	  {
		id: 16,
		pais: 'Ecuador',
		ciudad: 'Quito'
	  },
	  {
		id: 17,
		pais: 'Ecuador',
		ciudad: 'Esmeraldas'
	  },
	  {
		id: 18,
		pais: 'Ecuador',
		ciudad: 'Carchi'
	  },
	  {
		id: 19,
		pais: 'Argentina',
		ciudad: 'Buenos Aires'
	  },
	  {
		id: 20,
		pais: 'Argentina',
		ciudad: 'Rosario'
	  },
	  {
		id: 21,
		pais: 'Argentina',
		ciudad: 'La Plata'
	  },
	  {
		id: 22,
		pais: 'Chile',
		ciudad: 'Santiago'
	  },
	  {
		id: 23,
		pais: 'Chile',
		ciudad: 'Viña del mar'
	  },
	  {
		id: 24,
		pais: 'Chile',
		ciudad: 'Valdivia'
	  }
  
]

const GetAll = ()=> ciudadesList;


module.exports = {
	GetAll	
}