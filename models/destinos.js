

let destinosList = [];

const AllDestino = ()=> destinosList;

const AddDestino = (destino) => {

	destinosList = [...destinosList, destino];
	marcarFavorito(destino);
	return destino;
};

const marcarFavorito = destino => {
	destinosList = destinosList.map(des => {
		if( des.id === destino.id ){
			return destino;
		}
		else{
			return {...des, selected:false}
		}
	});
}

const SearchByName = param =>{
	
	const nuevosDestinos = destinosList.filter( option => {
		return option.ciudad.toLowerCase().indexOf(param.toLowerCase()) === 0;
	  });

	  return nuevosDestinos;

}

const SearchById = param =>{
	
	const odestino = destinosList.find( option => {
		return option.id == param;
	  });

	  return odestino;
}

const RemoveDestino = (id) => {
	destinosList = destinosList.filter(d => d.id !== id);
	console.log(destinosList);
	return id;
} ;

const UpdateDestino = (destino) => {
	
	   destinosList = destinosList.map(des => {
		if(des.id === destino.id){
			return destino;
		}
		else{
			return {...des};
		}
	})	
	return destino;
} ;

const ResetVotos = () => {	
	destinosList = destinosList.map(des => {
	 	return {...des, votos: 0}; 
 })	
 return true;
} ;

module.exports = {
	AllDestino,
	AddDestino,
	SearchByName,
	SearchById,
	RemoveDestino,
	UpdateDestino,
	ResetVotos,
	marcarFavorito
}